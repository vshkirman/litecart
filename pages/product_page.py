from pages.base_page import BasePage


class ProductPage(BasePage):
    ADD_TO_CART_BUTTON_LOCATOR = "//button[@name='add_cart_product']"
    SIZE_LOCATOR = "//select[@name='options[Size]']/option[@value='{}']"
    OPTIONS_SIZE_LOCATOR = "//select[@name='options[Size]']"

    def click_add_to_cart_button(self, size=None):
        if self.is_element_present(self.OPTIONS_SIZE_LOCATOR):
            self.select_size(size)
        self.click(self.ADD_TO_CART_BUTTON_LOCATOR)

    def select_size(self, size):
        self.click(self.SIZE_LOCATOR.format(size))
