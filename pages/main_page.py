from pages.login_module import LoginModule
from pages.header_module import HeaderModule
from pages.base_page import BasePage


class MainPage(BasePage):
    URL = "http://localhost/litecart/en/"
    CATEGORY_LOCATOR = "//div[@id='box-category-tree']//child::a[text()='{}']"

    @property
    def login_module(self):
        return LoginModule(self.driver)

    @property
    def header_module(self):
        return HeaderModule(self.driver)

    def open(self):
        self.navigate(self.URL)

    def click_on_category(self, category_name):
        self.click(self.CATEGORY_LOCATOR.format(category_name))
