from pages.base_page import BasePage
import re


class HeaderModule(BasePage):
    PRICE_IN_CART_LOCATOR = "//span[@class='formatted_value']"
    NOT_ZERO_QUANTITY_ITEM_CART_LOCATOR = "//span[@class='quantity' and text()!='0']"
    CHECKOUT_LOCATOR = "//span[@class='quantity']"

    def click_checkout_button(self):
        self.is_element_present(self.NOT_ZERO_QUANTITY_ITEM_CART_LOCATOR)
        self.click(self.CHECKOUT_LOCATOR)

    def get_preview_price(self):
        self.is_element_present(self.NOT_ZERO_QUANTITY_ITEM_CART_LOCATOR)
        price_with_currency = self.get_text_element(self.PRICE_IN_CART_LOCATOR)
        price_without_currency = re.findall(r"(\d*\.\d+|\d+)", price_with_currency)[0]
        return float(price_without_currency)
