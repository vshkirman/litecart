from pages.base_page import BasePage


class AccountPage(BasePage):
    FIRST_NAME_LOCATOR = "//input[@name='firstname']"
    SAVE_BUTTON_LOCATOR = "//button[@name='save']"

    def edit_first_name(self, first_name):
        self.clear_input_enter_text(self.FIRST_NAME_LOCATOR, first_name)
        self._click_save_button()

    def _click_save_button(self):
        self.click(self.SAVE_BUTTON_LOCATOR)
