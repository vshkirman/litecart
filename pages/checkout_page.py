from pages.base_page import BasePage
import re


class CheckoutPage(BasePage):
    ITEM_QUANTITY_LOCATOR = "//input[@name='quantity']"
    UPDATE_CART_ITEM_LOCATOR = "//button[@name='update_cart_item']"
    REMOVE_CART_ITEM_LOCATOR = "//button[@name='remove_cart_item']"
    ITEM_QUANTITY_ORDER_CONFIRMATION_LOCATOR = "//div[@id='order_confirmation-wrapper']//child::td[text()='{}']"
    ITEM_TOTAL_PRICE_LOCATOR = "//td[@class='sum']"
    IMAGE_WRAPPER_LOCATOR = "//a[@class='image-wrapper shadow']"

    def change_item_quantity(self, quantity):
        self.clear_input_enter_text(self.ITEM_QUANTITY_LOCATOR, quantity)
        self.click_update_cart_item(self.UPDATE_CART_ITEM_LOCATOR)

    def click_update_cart_item(self, locator):
        self.click(locator)

    def click_remove_cart_item(self):
        self.click(self.REMOVE_CART_ITEM_LOCATOR)

    def get_item_quantity(self, quantity):
        return int(self.get_text_element(self.ITEM_QUANTITY_ORDER_CONFIRMATION_LOCATOR.format(quantity)))

    def get_total_price(self, item_quantity, item_price):
        total_price_with_currency = self.wait_for_text(self.ITEM_TOTAL_PRICE_LOCATOR, f"{item_quantity * item_price}")
        total_price_without_currency = re.findall(r"(\d*\.\d+|\d+)", total_price_with_currency)[0]
        return float(total_price_without_currency)

    def is_cart_empty(self):
        cart_status = self.is_element_disappear(self.IMAGE_WRAPPER_LOCATOR)
        if cart_status:
            return True
        return False
