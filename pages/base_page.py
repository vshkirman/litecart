from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def navigate(self, url):
        self.driver.get(url)

    def _find_element(self, locator, time_out_sec=2):
        by_and_locator = self._define_type_locator(locator)
        element = WebDriverWait(self.driver, time_out_sec).until(
            ec.presence_of_element_located(by_and_locator))
        return element

    def _wait_for_element_to_disappear(self, locator, time_out_sec=2):
        by_and_locator = self._define_type_locator(locator)
        element = WebDriverWait(self.driver, time_out_sec).until(
            ec.invisibility_of_element_located(by_and_locator))
        return element

    def is_element_disappear(self, locator):
        if self._wait_for_element_to_disappear(locator):
            return True

    def is_element_present(self, locator):
        try:
            self._find_element(locator)
            return True
        except TimeoutException:
            return False

    def is_text_expected(self, locator, expected_text=None, time_out_sec=2):
        by_and_locator = self._define_type_locator(locator)
        is_text_expected = WebDriverWait(self.driver, time_out_sec).until(
            ec.text_to_be_present_in_element(by_and_locator, expected_text))

        return is_text_expected

    def get_text_element(self, locator):
        element = self._find_element(locator)
        return element.text

    def wait_for_text(self, locator, text):
        if self.is_text_expected(locator, text):
            return self.get_text_element(locator)

    def _define_type_locator(self, locator):
        if '//' in locator:
            # CSS : a[href="https://abc]
            # XPATH: .//div
            # XPATH (//div)
            return By.XPATH, locator

        return By.CSS_SELECTOR, locator

    def enter_text(self, locator, text):
        element = self._find_element(locator)
        element.send_keys(text)

    def click(self, locator):
        element = self._find_element(locator)
        element.click()

    def clear_input_enter_text(self, locator, text):
        element = self._find_element(locator)
        element.clear()
        element.send_keys(text)
