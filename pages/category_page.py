from pages.base_page import BasePage


class CategoryPage(BasePage):
    ITEM_LOCATOR = "//ul[contains(@class,'products')]//a[@class='link' and contains(@title,'{}')]"

    def click_item(self, item_name):
        self.click(self.ITEM_LOCATOR.format(item_name))
