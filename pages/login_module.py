from pages.base_page import BasePage


class LoginModule(BasePage):
    EMAIL_INPUT_LOCATOR = "//input[@name='email']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//button[@name='login']"
    EDIT_ACCOUNT_LOCATOR = "//aside//a[contains(@href, 'edit_account')]"

    def login(self, email, password):
        self.enter_email(email)
        self.enter_password(password)
        self.click_login_button()

    def enter_email(self, email):
        self.enter_text(self.EMAIL_INPUT_LOCATOR, email)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def click_edit_account(self):
        self.click(self.EDIT_ACCOUNT_LOCATOR)
