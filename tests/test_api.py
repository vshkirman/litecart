import pytest


class TestUserApi:
    @pytest.mark.xfail(reason="Unstable API")
    def test_create_user(self, user_requests):
        username = 'jsmith1'
        firstname = 'Vlad'
        lastname = 'Smith'
        email = 'jsmith@petstore.com'
        password = 'Qwer1234!'
        phone = '+375123456789'
        user_requests.create_user(username, firstname, lastname, email, password, phone)
        response_user_data = user_requests.get_user_by_username(username)
        actual_username = response_user_data.get('username')
        assert actual_username == username, \
            f'Actual username is {actual_username} while {username} expected'

    @pytest.mark.xfail(reason="Unstable API")
    def test_update_firstname_by_username(self, user_requests):
        username = 'jsmith1'
        new_firstname = 'Jameson'
        response_user_data = user_requests.get_user_by_username(username)
        user_requests.update_firstname_by_username(response_user_data, username, new_firstname)
        updated_response_user_data = user_requests.get_user_by_username(username)
        actual_firstname = updated_response_user_data.get('firstName')
        assert actual_firstname == new_firstname, \
            f'Actual first name is {actual_firstname} while {new_firstname} expected'
