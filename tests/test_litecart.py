import pytest
import allure


@pytest.mark.usefixtures("open_homepage")
@pytest.mark.usefixtures("attach_screenshot_to_allure_report")
class TestLitecart:
    @allure.title('Test change first name')
    @pytest.mark.db
    @pytest.mark.frontend
    @allure.step('change first name of the user')
    def test_change_first_name(self, customer_table, main_page, account_page):
        new_first_name = 'Mike'
        main_page.login_module.login('j.smith@mailforspam.com', '12345')
        main_page.login_module.click_edit_account()
        account_page.edit_first_name(new_first_name)
        actual_first_name = customer_table.get_first_name_by_email('j.smith@mailforspam.com')
        assert actual_first_name == new_first_name, f'First Name in DB {actual_first_name} ' \
                                                    f'while {new_first_name} expected'

    @allure.title('Test prices are correct')
    @pytest.mark.frontend
    @allure.step('check that total price is counted correct')
    def test_prices_of_items_in_cart(self, main_page, category_page, product_page, checkout_page, item_quantity=3):
        main_page.click_on_category('Rubber Ducks')
        category_page.click_item('Yellow Duck')
        product_page.click_add_to_cart_button(size='Medium')
        item_price = main_page.header_module.get_preview_price()
        main_page.header_module.click_checkout_button()
        checkout_page.change_item_quantity(item_quantity)
        actual_total_price = checkout_page.get_total_price(item_quantity, item_price)
        assert actual_total_price == item_quantity * item_price, \
            f'Actual total price is {actual_total_price} while {item_quantity * item_price} is expected'

    @allure.title('Test removing items from cart')
    @pytest.mark.frontend
    @allure.step('remove added item from cart')
    def test_remove_item_from_cart(self, main_page, category_page, checkout_page, product_page):
        main_page.click_on_category('Rubber Ducks')
        category_page.click_item('Blue Duck')
        product_page.click_add_to_cart_button()
        main_page.header_module.click_checkout_button()
        checkout_page.click_remove_cart_item()
        assert checkout_page.is_cart_empty(), 'Cart is not empty'

    @allure.title('Test quantity item in cart')
    @pytest.mark.frontend
    @allure.step('count the quantity of items in cart')
    def test_quantity_item_in_cart(self, main_page, category_page, product_page, checkout_page, item_quantity=3):
        main_page.click_on_category('Rubber Ducks')
        category_page.click_item('Green Duck')
        product_page.click_add_to_cart_button()
        main_page.header_module.click_checkout_button()
        checkout_page.change_item_quantity(item_quantity)
        actual_item_quantity = checkout_page.get_item_quantity(item_quantity)
        assert actual_item_quantity == item_quantity, f'Actual quantity of item(s) is {actual_item_quantity}' \
                                                      f'while {item_quantity} expected'
