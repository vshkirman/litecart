import pytest
import allure
from mysql.connector import connect
from sql.customer_table import CustomerTable
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from pages.main_page import MainPage
from pages.account_page import AccountPage
from pages.product_page import ProductPage
from pages.category_page import CategoryPage
from pages.checkout_page import CheckoutPage
from api_requests.user_requests import UserRequests


@pytest.fixture(scope="session")
def connection_to_test_db():
    connection = connect(
        host="localhost",
        user='root',
        database="litecart"
    )

    yield connection
    connection.close()


@pytest.fixture()
@allure.title('Connect to lc_customers table')
def customer_table(connection_to_test_db):
    return CustomerTable(connection_to_test_db)


@pytest.fixture()
def chromedriver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    # chrome_options.add_argument('start-maximized')

    return webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)


@pytest.fixture()
@allure.title('Open main page')
def open_homepage(chromedriver):
    main_page = MainPage(chromedriver)
    main_page.open()


@pytest.fixture()
@allure.title('Create main page fixture')
def main_page(chromedriver):
    return MainPage(chromedriver)


@pytest.fixture()
@allure.title('Create account page fixture')
def account_page(chromedriver):
    return AccountPage(chromedriver)


@pytest.fixture()
@allure.title('Create product page fixture')
def product_page(chromedriver):
    return ProductPage(chromedriver)


@pytest.fixture()
@allure.title('Create category page fixture')
def category_page(chromedriver):
    return CategoryPage(chromedriver)


@pytest.fixture()
@allure.title('Create checkout page fixture')
def checkout_page(chromedriver):
    return CheckoutPage(chromedriver)


@pytest.fixture(scope='session')
@allure.title('Create user requests fixture')
def user_requests():
    return UserRequests()


@pytest.fixture
@allure.title('Screenshot')
def attach_screenshot_to_allure_report(chromedriver, request):
    yield
    chromedriver.maximize_window()
    screenshot_name = f"{request.node.name}.png"
    chromedriver.save_screenshot(screenshot_name)

    allure.attach.file(screenshot_name, attachment_type=allure.attachment_type.PNG)

    chromedriver.quit()

    # driver_variable = webdriver.Chrome(ChromeDriverManager().install())
    # yield driver_variable
    # driver_variable.quit()
