class SqlService:
    def __init__(self, connection):
        self.connection = connection
        self.cursor = self.connection.cursor()

    def execute_select_query(self, query):
        self.execute_query(query)
        return self.cursor.fetchall()

    def execute_query(self, query):
        self.cursor.execute(query)
