from sql.sql_service import SqlService


class CustomerTable(SqlService):
    FIRSTNAME_BY_EMAIL_QUERY = "SELECT firstname FROM `lc_customers` WHERE email = '{}'"

    def get_first_name_by_email(self, email):
        query = self.FIRSTNAME_BY_EMAIL_QUERY.format(email)
        return self.execute_select_query(query)[0][0]
