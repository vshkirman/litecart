import json
from api_requests.base_requests import BaseRequests


class UserRequests(BaseRequests):

    def create_user(self, username, firstname, lastname, email, password, phone, status_code=200):
        request_data = json.dumps({
            # "id": 1,
            "username": username,
            "firstName": firstname,
            "lastName": lastname,
            "email": email,
            "password": password,
            "phone": phone,
            "userStatus": 1
        })
        response = self.post('/user', request_data)
        assert response.status_code == status_code, \
            f'Status code is {response.status_code} while {status_code} expected'
        return response

    def get_user_by_username(self, username):
        response = self.get('/user/{}'.format(username))
        if response.status_code == 200:
            data = json.loads(response.text)
            return data

    def update_firstname_by_username(self, request_data, username, new_firstname, status_code=200):
        data = json.dumps({
            "id": request_data.get('id'),
            "username": request_data.get('username'),
            "firstName": new_firstname,
            "lastName": request_data.get('lastName'),
            "email": request_data.get('email'),
            "password": request_data.get('password'),
            "phone": request_data.get('phone'),
            "userStatus": 1
        })
        response = self.put('/user/{}'.format(username), data)
        assert response.status_code == status_code, \
            f'Status code is {response.status_code} while {status_code} expected'
        return response
