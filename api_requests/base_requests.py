import requests


class BaseRequests:
    def __init__(self):
        self.user_url = 'https://petstore.swagger.io/v2{}'
        self.petstore_headers = {
            'Content-Type': 'application/json'
        }

    def post(self, endpoint, data=None):
        url = self.user_url.format(endpoint)
        response = requests.post(url, headers=self.petstore_headers, data=data)
        return response

    def get(self, endpoint):
        url = self.user_url.format(endpoint)
        response = requests.get(url, headers=self.petstore_headers)
        return response

    def put(self, endpoint, data=None):
        url = self.user_url.format(endpoint)
        response = requests.put(url, headers=self.petstore_headers, data=data)
        return response
